function st = m2d_addSyntheticNoise(st,stNoise)
%
% m2d_addSyntheticNoise(st,stNoise) adds synthetic Gaussian noise to the
% MARE2DEM response data in input structure st. 
% 
% Note: This is a low level workhorse function. Most users will want to 
% instead use function m2d_makeSyntheticData() since it 
% handles the required file i/o.
%
% Arguments:
%
%   st  - structure containing the MARE2DEM responses (MT and/or CSEM) read
%   in using m2d_readEMData2DFile.
%
%   stNoise  - structure specifying noise levels to use. You only need to
%              specify the fields for the data type in the input file 
%              (i.e. if you only have MT data, you only need to specify 
%              stNoise.mt.relNoise). Fields:
%
%     stNoise.mt.relNoise  - relative noise to add to MT responses
%                            (e.g. 0.05 means add 5% Gaussian noise). Note
%                            this is the relative error in apparent
%                            resistivity. Phase noise will be 0.5*relNoise.
%                            ( as can be shown by propagation of error from
%                            the impedance to app. res. and phase).
% 
%     stNoise.csem.relNoiseE - relative noise to add to CSEM electric field 
%                              responses (e.g. 0.05 means add 5% Gaussian noise).
%
%     stNoise.csem.minAmpE - Electric field responses below this amplitude 
%                            are omitted from the output data file.
%                          
%     stNoise.csem.relNoiseB - relative noise to add to CSEM magnetic field 
%                              responses (e.g. 0.05 means add 5% Gaussian noise).
%
%     stNoise.csem.minAmpB - Magnetic field responses below this amplitude 
%                            are omitted from the output data file.
%
% Outputs:
%
%   st - structure copied from input structure st. 
%       Synthetic noisy data generated from the input forward responses 
%       (i.e. st.DATA(:,7) ) are copied into output data (st.DATA(:,5)) 
%       with standard error (st.DATA(:,6);
% 
%
% Kerry Key
% Lamont-Doherty Earth Observatory
%   


%
% Check input arguments
%
    % kwk debug: to code...

%
% Add noise to any input MT responses:
%
st = sub_mt_noise(st,stNoise);


%
% Add noise to any input CSEM responses:
%

st = sub_csem_noise(st,stNoise);

%
% Now trim off response data so that output st.DATA only has data
% parameters (columns 1:4) and the data and standard error (columns 5:6):
%
st.DATA = st.DATA(:,1:6);
 

%--------------------------------------------------------------------------
function st = sub_mt_noise(st,stNoise)

    % return if no MT data:
    if ~isfield(st,'stMT') || isempty(st.stMT)
        return
    end

    %
    % Peel off MT data and then add noise to various MT data types, if
    % present:
    %
    stMT = st.stMT;
    DATA = st.DATA;
 
    %
    % Log10 apparent resistivity:
    %
    ltype = DATA(:,1) == 123 | DATA(:,1) == 125;  % data codes for TE and TM log10 ApRes    
     
    if ~isempty(ltype)
        d = DATA(ltype,7);
        [DATA(ltype,5), DATA(ltype,6)] = logamplitude_noise(d,stNoise.mt.relNoise);
    end
 
    
    %
    % Apparent resistivity:
    %
    ltype = DATA(:,1) == 103 | DATA(:,1) == 105;  % data codes for TE and TM ApRes    
     
    if ~isempty(ltype)
        d = DATA(ltype,7);
        [DATA(ltype,5), DATA(ltype,6)] = amplitude_noise(d,stNoise.mt.relNoise);
    end
 
    
    %
    % Phase:
    %
    ltype = DATA(:,1) == 104 | DATA(:,1) == 106;  % TE and TM phases
    
    if ~isempty(ltype)
        d = DATA(ltype,7);
        relNoise = stNoise.mt.relNoise/2; % note phase error is 1/2 apparent resistivity error since apres ~ Z^2
        [DATA(ltype,5), DATA(ltype,6)] = phase_noise(d,relNoise);
    end
 
    
    %
    % Tipper ! kwk debug: to do!
    %
     
    
    %
    % Insert modified DATA into output structure:
    %
    st.DATA = DATA;
    
end

%--------------------------------------------------------------------------
function st = sub_csem_noise(st,stNoise)

    % return if no CSEM data:
    if ~isfield(st,'stCSEM') || isempty(st.stCSEM)
        return
    end

    %
    % Peel off MT data and then add noise to various MT data types, if
    % present:
    %
    stCSEM = st.stCSEM;
    DATA   = st.DATA;
 
    %
    % Log10 amplitude:
    %
    ltype = ismember(DATA(:,1),[27,28,29,37,38,39] );     
     
    if ~isempty(ltype)
        d = DATA(ltype,7);
        [DATA(ltype,5),  DATA(ltype,6)] = logamplitude_noise(d,stNoise.csem.relNoise);  
    end
  
    
    %
    % amplitude
    %
    ltype = ismember(DATA(:,1),[21,23,25,31,33,35]);   
     
    if ~isempty(ltype)     
        d = DATA(ltype,7);
        [DATA(ltype,5),  DATA(ltype,6)] = amplitude_noise(d,stNoise.csem.relNoise);  
    end
        
    %
    % Phase:
    %
    ltype = ismember(DATA(:,1),[22,24,26,32,34,36]);   
    
    if ~isempty(ltype)
        d = DATA(ltype,7);
        [DATA(ltype,5),  DATA(ltype,6)] = phase_noise(d,stNoise.csem.relNoise);  
    end
    
    %
    % Real and Imaginary data: 
    %
    ltype = ismember(DATA(:,1),[1:6 11:16]);  
    
    if any(ltype)
        beep;

        h = warndlg('Error! ','m2d_addSyntheticNoise does not yet support real & imaginary format data, sorry! ');
        set(h,'windowstyle','modal')
        waitfor(h);
        return
    end
    
    %
    % Insert modified DATA into output structure:
    %
    st.DATA = DATA;
    
end

%--------------------------------------------------------------------------
function [logamp, std]  = logamplitude_noise(logamp,relError)
    
    std = 0.4343*relError;  % log10 relative error  
    
    [logamp, std] = apply_noise(logamp,std);
    
end

%--------------------------------------------------------------------------
function [amp, std] = amplitude_noise(amp,relError)
    
    std = relError*abs(amp);  % convert relative to absolute error  
   
    [amp, std] = apply_noise(amp,std);
   
end

%--------------------------------------------------------------------------
function [phs, std] = phase_noise(phs,relError)
    
    std = relError*180/pi;
    
    [phs, std] = apply_noise(phs,std);
      
end

%--------------------------------------------------------------------------
function [d, std] = apply_noise(d,std)
    
    % Generates and adds random Gaussian noise with standard deviation std
    % to input data d
          
    noise = randn(size(d)); % unit Gaussian noise
    
    % limit noise to 2 standard deviations in case randn was unlucky:
    iLarge        = abs(noise) > 2;     
    noise(iLarge) = sign(noise(iLarge))*2;
    
    % scale unit noise by std:
    noise = std.*noise;   
    
    % add noise:
    d = d + noise;
    
    std = std.*ones(size(d));
    
end

end

