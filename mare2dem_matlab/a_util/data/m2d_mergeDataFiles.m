function m2d_mergeDataFiles(sMTDataFile,sCSEMDataFile,sOutputFileName)
%
% Function that merges separate MT and CSEM data files into a
% single file for joint inversion. 
%
% KWk debug: this could be made more general to merge any data types in the
% data files, rather than requiring one to have MT data and the other CSEM.

% Read MT data file:
stM = m2d_readEMData2DFile(sMTDataFile);

stUTM_MT = stM.stUTM;
stMT_MT  = stM.stMT;
DATA_MT  = stM.DATA;
 
if isempty(DATA_MT)
    str = sprintf('Error loading MT data file %s.\n Is the path and name correct?\n\n Try again bucko!',sMTDataFile);
    hErr = errordlg(str,'m2d_mergeDataFiles Error','modal');
    waitfor(hErr)
    return
end

% Read CSEM Data file:
stC = m2d_readEMData2DFile(sCSEMDataFile);

stUTM_CSEM  = stC.stUTM;
stCSEM_CSEM = stC.stCSEM;
DATA_CSEM   = stC.DATA;
 
if isempty(DATA_CSEM)
    str = sprintf('Error loading CSEM data file %s.\n Is the path and name correct?\n\n Try again bucko!',sCSEMDataFile);
    hErr = errordlg(str,'m2d_mergeDataFiles Error','modal');
    waitfor(hErr)
    return
end

% Check that the UTM0 and 2D strike are identical:

if stUTM_MT.north0 ~= stUTM_CSEM.north0 || ...
   stUTM_MT.east0 ~= stUTM_CSEM.east0  || ...
   stUTM_MT.theta ~= stUTM_CSEM.theta 

    str = sprintf('Error, disagreement found in the UTM northing or easting or 2D strike angle for the MT and CSEM files. They must be the same. \n\nTry again bucko!');
    hErr = errordlg(str,'m2d_mergeDataFiles Error','modal');
    waitfor(hErr)
    return
end

% Merge Data arrays:

stOut.stUTM  = stUTM_MT;
stOut.stMT   = stMT_MT;
stOut.stCSEM = stCSEM_CSEM;
stOut.DATA = [DATA_MT; DATA_CSEM];


% Save:
stOut.sComment = sprintf('MT and CSEM merged data file created with m2d_mergeDataFiles.m on %s',datestr(now));
m2d_writeEMData2DFile(sOutputFileName,stOut) 

% Goodbye message:
% h = msgbox('All done merging the MT and CSEM data files.','m2d_mergeDataFiles.m','modal');
% waitfor(h);

end